package com.example.cardswithcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Card
import androidx.compose.material.ContentAlpha
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.Top
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.example.cardswithcompose.ui.theme.CardsWithComposeTheme

class MainActivity : ComponentActivity() {
    @ExperimentalCoilApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CardsWithComposeTheme {
                /*UserListScreen()
                UserProfileScreen()*/
                NavigationUtil()
            }
        }
    }
}

@ExperimentalCoilApi
@Composable
fun UserListScreen(navHostController: NavHostController) {
    Scaffold(topBar = {
        AppBar(
            title = "Users",
            icon = Icons.Default.Home
        ) {}
    }) {
        Surface(modifier = Modifier.fillMaxSize(), color = Color.DarkGray) {
            LazyColumn {
                items(userProfileList) {
                    ProfileCard(userProfile = it) {
                        navHostController.navigate(route = "profile/${it.id}") {
                            popUpTo(route = "users")
                        }
                    }
                }
            }
        }
    }
}

@ExperimentalCoilApi
@Composable
fun UserProfileScreen(userId: Int, navHostController: NavHostController) {
    val userProfile =
        userProfileList.first {
            userId == it.id
        }
    Scaffold(topBar = {
        AppBar(
            title = "Profile",
            icon = Icons.Default.ArrowBack
        ) {
            /* simplest way to go back
            navHostController.navigateUp() */

            /* this syntax means: navigate to the "users” destination
               only if i'm not already on the "users" destination,
               avoiding multiple copies on the top of the back stack */
            navHostController.navigate(route = "users") {
                launchSingleTop = true
            }
        }
    }) {
        Surface(modifier = Modifier.fillMaxSize(), color = Color.DarkGray) {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = CenterHorizontally,
                verticalArrangement = Arrangement.Top
            ) {
                ProfilePicture(
                    image = userProfile.pictureUrl,
                    status = true,
                    imageSize = 260.dp
                )
                ProfileContent(
                    name = userProfile.name,
                    status = true,
                    alignment = CenterHorizontally
                )
            }
        }
    }
}

@Composable
fun AppBar(title: String, icon: ImageVector, onIconClick: () -> Unit) {
    TopAppBar(
        navigationIcon = {
            Icon(
                icon,
                contentDescription = "icon",
                modifier = Modifier
                    .padding(horizontal = 12.dp)
                    .clickable { onIconClick.invoke() }
            )
        },
        title = { Text(text = title) }
    )
}

@ExperimentalCoilApi
@Composable
fun ProfileCard(userProfile: UserProfile, onClick: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(top = 8.dp, bottom = 4.dp, start = 16.dp, end = 16.dp)
            .fillMaxWidth()
            .wrapContentHeight(align = Top)
            .clickable { onClick.invoke() },
        elevation = 8.dp
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            ProfilePicture(userProfile.pictureUrl, userProfile.status, 80.dp)
            ProfileContent(
                userProfile.name,
                userProfile.status,
                Alignment.Start
            )
        }
    }
}

@Composable
fun ProfileContent(
    name: String,
    status: Boolean,
    alignment: Alignment.Horizontal
) {
    Column(
        modifier = Modifier
            .padding(8.dp),
        horizontalAlignment = alignment
    ) {

        CompositionLocalProvider(
            LocalContentAlpha provides if (status) 1f else ContentAlpha.medium
        ) {
            Text(
                text = name,
                style = MaterialTheme.typography.h5
            )
        }

        CompositionLocalProvider(
            LocalContentAlpha provides ContentAlpha.medium
        ) {
            Text(
                text = if (status) "Active now" else "Offline",
                style = MaterialTheme.typography.body2
            )
        }

    }
}

@ExperimentalCoilApi
@Composable
fun ProfilePicture(image: String, status: Boolean, imageSize: Dp) {
    Card(
        shape = CircleShape,
        border = BorderStroke(
            2.dp,
            color = if (status) Color.Green else Color.Red
        ),
        modifier = Modifier.padding(16.dp),
        elevation = 4.dp
    ) {
        Image(
            painter = rememberImagePainter(data = image) {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_foreground)
                transformations(CircleCropTransformation())
            },
            contentDescription = "card picture",
            modifier = Modifier.size(imageSize)
        )
    }
}

@ExperimentalCoilApi
@Preview(showBackground = true)
@Composable
fun UserListPreview() {
    CardsWithComposeTheme(darkTheme = true) {
        UserListScreen(rememberNavController())
    }
}

@ExperimentalCoilApi
@Preview(showBackground = true)
@Composable
fun UserProfilePreview() {
    CardsWithComposeTheme(darkTheme = true) {
        UserProfileScreen(2, rememberNavController())
    }
}