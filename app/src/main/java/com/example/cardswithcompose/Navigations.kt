package com.example.cardswithcompose

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi

@ExperimentalCoilApi
@Composable
fun NavigationUtil() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = "users"
    ) {
        composable(route = "users") { UserListScreen(navController) }
        composable(
            route = "profile/{userId}",
            arguments = listOf(navArgument("userId") { type = NavType.IntType })
        )
        {
            UserProfileScreen(it.arguments!!.getInt("userId"), navController)
        }
    }
}