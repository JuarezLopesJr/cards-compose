package com.example.cardswithcompose.ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

/* customizing colors in MaterialTheme */
val LightGreen200 = Color(0x9932cd32)

/* to invoke in code MaterialTheme.colors.lightGreen */
val Colors.lightGreen: Color
    @Composable
    get() = LightGreen200